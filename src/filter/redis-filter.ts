import * as redis from 'redis';
import { BaseFilter } from '.';
import { Config, defaultConfig } from '../config';
import { DAY_IN_SECONDS, nowInSeconds } from '../utils';
import { promisify } from 'util';
const bloomxx = require('bloomxx');

export class RedisFilter implements BaseFilter {
  private config: Config;
  private lastDayIndex: number;
  private filters: Map<number, any>;
  private redis: redis.RedisClient;

  constructor(config: Config = defaultConfig) {
    this.config = config;
    this.lastDayIndex = Math.floor(nowInSeconds() / DAY_IN_SECONDS);
    this.filters = new Map<number, any>();
    this.redis = redis.createClient(this.config.redisOptions);
  }

  public async initialize() {
    const key = `${this.config.redisOptions?.key},dayIndex`;
    const get = promisify(this.redis.get).bind(this.redis);

    const dayIndex = await get(key);
    if (dayIndex) {
      this.lastDayIndex = Number(dayIndex);
    }
    return true;
  }

  public async add(token: string, exp: number): Promise<boolean> {
    const dayIndex = Math.floor(exp / DAY_IN_SECONDS);
    if (!this.filters.has(dayIndex)) {
      const options = {
        ...this.config.redisOptions,
        key: `${this.config.redisOptions?.key},${dayIndex}`,
        redis: this.redis,
      };
      const filter = bloomxx.RedisFilter.createOptimal(
        this.config.daySize,
        this.config.errorRate,
        options,
      );
      const initialize = promisify(filter.initialize).bind(filter);
      await initialize();
      this.filters.set(dayIndex, filter);
    }

    const filter = this.filters.get(dayIndex);
    const add = promisify(filter.add).bind(filter);
    await Promise.all([add(token), this.clearUnused()]);
    return true;
  }

  public async has(token: string, exp: number): Promise<boolean> {
    const dayIndex = Math.floor(exp / DAY_IN_SECONDS);
    if (!this.filters.has(dayIndex)) {
      const options = {
        ...this.config.redisOptions,
        key: `${this.config.redisOptions?.key},${dayIndex}`,
        redis: this.redis,
      };
      const filter = bloomxx.RedisFilter.createOptimal(
        this.config.daySize,
        this.config.errorRate,
        options,
      );
      const initialize = promisify(filter.initialize).bind(filter);
      await initialize();
      this.filters.set(dayIndex, filter);
    }
    const filter = this.filters.get(dayIndex);
    const has = promisify(filter.has).bind(filter);
    const [ok, _] = await Promise.all([has(token), this.clearUnused()]);
    return ok;
  }

  public async clear(): Promise<boolean> {
    const jobs: any[] = [];
    this.filters.forEach((filter) => {
      const del = promisify(filter.del).bind(filter);
      jobs.push(del());
    });
    await Promise.all(jobs);

    this.filters.clear();
    return true;
  }

  private async clearUnused() {
    const dayIndex = Math.floor(nowInSeconds() / DAY_IN_SECONDS);
    const jobs: any[] = [];
    for (let i = this.lastDayIndex; i < dayIndex; i++) {
      if (!this.filters.has(i)) {
        continue;
      }

      const filter = this.filters.get(i);
      const del = promisify(filter.del).bind(filter);
      jobs.push(del());
    }
    await Promise.all(jobs);

    for (let i = this.lastDayIndex; i < dayIndex; i++) {
      this.filters.delete(i);
    }
    if (this.lastDayIndex < dayIndex) {
      this.lastDayIndex = dayIndex;

      const key = `${this.config.redisOptions?.key},dayIndex`;
      const set = promisify(this.redis.set).bind(this.redis);
      await set(key, this.lastDayIndex.toString());
    }
  }
}
