import { BaseFilter } from '.';
import { Config, defaultConfig } from '../config';
import { DAY_IN_SECONDS, nowInSeconds } from '../utils';
const bloomxx = require('bloomxx');

export class MemoryFilter implements BaseFilter {
  private config: Config;
  private lastDayIndex: number;
  private filters: Map<number, any>;

  constructor(config: Config = defaultConfig) {
    this.config = config;
    this.lastDayIndex = Math.floor(nowInSeconds() / DAY_IN_SECONDS);
    this.filters = new Map<number, any>();
  }

  public async initialize() {
    return true;
  }

  public async add(token: string, exp: number): Promise<boolean> {
    const dayIndex = Math.floor(exp / DAY_IN_SECONDS);
    if (!this.filters.has(dayIndex)) {
      this.filters.set(
        dayIndex,
        bloomxx.BloomFilter.createOptimal(
          this.config.daySize,
          this.config.errorRate,
        ),
      );
    }
    const filter = this.filters.get(dayIndex);
    filter.add(token);
    this.clearUnused();
    return true;
  }

  public async has(token: string, exp: number): Promise<boolean> {
    const dayIndex = Math.floor(exp / DAY_IN_SECONDS);
    if (!this.filters.has(dayIndex)) {
      return false;
    }
    const filter = this.filters.get(dayIndex);
    this.clearUnused();
    return filter.has(token);
  }

  public async clear(): Promise<boolean> {
    this.filters.clear();
    return true;
  }

  private clearUnused() {
    const dayIndex = Math.floor(nowInSeconds() / DAY_IN_SECONDS);
    for (let i = this.lastDayIndex; i < dayIndex; i++) {
      this.filters.delete(i);
    }
    if (this.lastDayIndex < dayIndex) {
      this.lastDayIndex = dayIndex;
    }
  }
}
