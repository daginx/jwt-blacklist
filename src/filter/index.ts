export interface BaseFilter {
  add: (token: string, exp: number) => Promise<boolean>;
  has: (token: string, exp: number) => Promise<boolean>;
  clear: () => Promise<boolean>;
  initialize: () => Promise<boolean>;
}
