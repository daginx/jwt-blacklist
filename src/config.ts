export enum STORE_TYPE {
  MEMORY = 'memory',
  REDIS = 'redis',
}

export class RedisOptions {
  host?: string = '127.0.0.1';
  port?: number = 6379;
  key?: string = 'jwt-blacklist';
}

export class Config {
  daySize?: number = 10000;
  errorRate?: number = 0.001;
  storeType?: STORE_TYPE = STORE_TYPE.MEMORY;
  redisOptions?: RedisOptions = new RedisOptions();
}

export const defaultConfig: Config = {
  daySize: 10000,
  errorRate: 0.001,
  storeType: STORE_TYPE.MEMORY,
  redisOptions: new RedisOptions(),
};
