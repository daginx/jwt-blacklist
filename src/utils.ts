export function nowInSeconds(): number {
  return Math.floor(Date.now() / 1000);
}

export const DAY_IN_SECONDS = 86400;

export function isString(value: any) {
  return typeof value === 'string' || value instanceof String;
}
