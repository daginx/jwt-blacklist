import * as jwt from 'jsonwebtoken';
import { Config, defaultConfig, STORE_TYPE, RedisOptions } from './config';
import { nowInSeconds } from './utils';
import { BaseFilter } from './filter';
import { MemoryFilter } from './filter/memory-filter';
import { RedisFilter } from './filter/redis-filter';

export class BlackList {
  private config: Config;
  private filter: BaseFilter;

  constructor(config: Config = defaultConfig) {
    this.config = {
      ...defaultConfig,
      ...config,
    };
    this.config.redisOptions = {
      ...new RedisOptions(),
      ...this.config.redisOptions,
    };

    if (this.config.storeType === STORE_TYPE.MEMORY) {
      this.filter = new MemoryFilter(this.config);
    } else {
      this.filter = new RedisFilter(this.config);
    }
  }

  public async add(
    token: string,
    options?: jwt.DecodeOptions,
  ): Promise<boolean> {
    const decoded = jwt.decode(token, options);
    if (!decoded) {
      return false;
    }
    if (typeof decoded === 'string' || decoded instanceof String) {
      return false;
    }

    const exp = decoded.exp || 0;
    const now = nowInSeconds();
    if (exp < now) {
      return false;
    }

    return this.filter.add(token, exp);
  }

  public async has(
    token: string,
    options?: jwt.DecodeOptions,
  ): Promise<boolean> {
    const decoded = jwt.decode(token, options);
    if (!decoded) {
      return false;
    }
    if (typeof decoded === 'string' || decoded instanceof String) {
      return false;
    }

    const exp = decoded.exp || 0;
    const now = nowInSeconds();
    if (exp < now) {
      return false;
    }

    return this.filter.has(token, exp);
  }

  public async clear() {
    return this.filter.clear();
  }

  public getFilter() {
    return this.filter;
  }
}

export async function createBlackList(config: Config = defaultConfig) {
  const blackList = new BlackList(config);
  await blackList.getFilter().initialize();
  return blackList;
}
