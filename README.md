# JWT Black List

A black list for JWT base on bloom filter and xxhash

## Install

```bash
npm install jwt-blacklist
```

## TL;DR;

From version 0.1.0, jwt-blacklist is actually a black list instead of an extension of [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken). It also supports Typescript and Redis now.

You can see older docs on [Github](https://github.com/nesso99/jwt-blacklist)

## Usage

### Create a blacklist

```typescript
import { createBlackList } from 'jwt-blacklist';

// memory
const blacklist = await createBlackList({
  daySize: 10000, // optional, number of tokens need revoking each day
  errorRate: 0.001, // optional, error rate each day
});

// redis
const blacklist = await createBlackList({
  daySize: 10000, // optional, number of tokens need revoking each day
  errorRate: 0.001, // optional, error rate each day
  storeType: 'redis', // store type
  redisOptions: {
    host: 'localhost',
    port: 6379,
    key: 'jwt-blacklist', // optional: redis key prefix
  }, // optional, redis options
});
```

### Add, has and clear

```typescript
blacklist.add(token); // not allow jwt without exp
blacklist.has(token);
blacklist.clear();
```
