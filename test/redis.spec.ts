import * as jwt from 'jsonwebtoken';
import { BlackList, createBlackList, STORE_TYPE } from '../src';
import { nowInSeconds, DAY_IN_SECONDS } from '../src/utils';

const redisHost = process.env.REDIS_HOST || 'localhost';

describe('Redis Test', () => {
  let blacklist: BlackList;

  beforeEach(async () => {
    blacklist = await createBlackList({
      storeType: STORE_TYPE.REDIS,
      redisOptions: { host: redisHost },
    });
  });

  test('other config', async () => {
    const otherBlackList = await createBlackList({
      daySize: 1,
      errorRate: 1,
      storeType: STORE_TYPE.REDIS,
      redisOptions: { host: redisHost },
    });
  });

  test('add token', async () => {
    const exp = nowInSeconds() + 3600;
    const token = jwt.sign({ greate: true, exp }, 'secret');
    const ok = await blacklist.add(token);
    expect(ok).toBe(true);

    const notExpToken = jwt.sign({ greate: true }, 'secret');
    const notExpOk = await blacklist.add(notExpToken);
    expect(notExpOk).toBe(false);

    const stringToken = jwt.sign('greate', 'secret');
    const stringOk = await blacklist.add(stringToken);
    expect(stringOk).toBe(false);
  });

  test('has token', async () => {
    const exp = nowInSeconds();
    const token1 = jwt.sign({ greate: true, exp: exp + 3600 }, 'secret');
    const token2 = jwt.sign({ greate: true, exp: exp + 86400 }, 'secret');
    const token3 = jwt.sign({ greate: true, exp: exp + 86400 * 30 }, 'secret');
    const [ok1, ok2, ok3] = await Promise.all([
      blacklist.add(token1),
      blacklist.add(token2),
      blacklist.add(token3),
    ]);
    expect(ok1).toBe(true);
    expect(ok2).toBe(true);
    expect(ok3).toBe(true);

    const isBlackList = await blacklist.has(token1);
    expect(isBlackList).toBe(true);

    const otherToken = jwt.sign({ greate: true, exp }, 'other secret');
    const otherBlacklist = await blacklist.has(otherToken);
    expect(otherBlacklist).toBe(false);
  });

  test('big ttl', async () => {
    const exp = nowInSeconds() + 365 * DAY_IN_SECONDS;
    const token = jwt.sign({ greate: true, exp }, 'secret');
    const ok = await blacklist.add(token);
    expect(ok).toBe(true);

    const isBlackList = await blacklist.has(token);
    expect(isBlackList).toBe(true);
  });
});
