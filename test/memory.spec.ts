import * as jwt from 'jsonwebtoken';
import { BlackList, createBlackList, STORE_TYPE } from '../src';
import { nowInSeconds, DAY_IN_SECONDS } from '../src/utils';

describe('Memory Test', () => {
  let blacklist: BlackList;

  beforeEach(async () => {
    blacklist = await createBlackList();
  });

  test('other config', async () => {
    const otherBlackList = await createBlackList({
      daySize: 1,
      errorRate: 1,
    });
  });

  test('add token', async () => {
    const exp = nowInSeconds() + 3600;
    const token = jwt.sign({ greate: true, exp }, 'secret');
    const ok = await blacklist.add(token);
    expect(ok).toBe(true);

    const notExpToken = jwt.sign({ greate: true }, 'secret');
    const notExpOk = await blacklist.add(notExpToken);
    expect(notExpOk).toBe(false);

    const stringToken = jwt.sign('greate', 'secret');
    const stringOk = await blacklist.add(stringToken);
    expect(stringOk).toBe(false);
  });

  test('has token', async () => {
    const exp = nowInSeconds() + 3600;
    const token = jwt.sign({ greate: true, exp }, 'secret');
    const ok = await blacklist.add(token);
    expect(ok).toBe(true);

    const isBlackList = await blacklist.has(token);
    expect(isBlackList).toBe(true);

    const otherToken = jwt.sign({ greate: true, exp }, 'other secret');
    const otherBlacklist = await blacklist.has(otherToken);
    expect(otherBlacklist).toBe(false);
  });

  test('big ttl', async () => {
    const exp = nowInSeconds() + 365 * DAY_IN_SECONDS;
    const token = jwt.sign({ greate: true, exp }, 'secret');
    const ok = await blacklist.add(token);
    expect(ok).toBe(true);

    const isBlackList = await blacklist.has(token);
    expect(isBlackList).toBe(true);
  });
});
